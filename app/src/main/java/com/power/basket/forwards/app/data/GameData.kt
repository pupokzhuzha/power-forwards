package com.power.basket.forwards.app.data

data class GameData(
    val name : String,
    val image : Int,
    val desc : String
)
