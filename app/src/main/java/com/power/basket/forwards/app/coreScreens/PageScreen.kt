package com.power.basket.forwards.app.coreScreens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.power.basket.forwards.app.data.GameData
import com.power.basket.forwards.app.data.ListPage
import com.power.basket.forwards.app.databinding.ScreenPageBinding

class PageScreen : Fragment() {

    private var binding: ScreenPageBinding? = null
    private val mBinding get() = binding!!

    private var page = 1
    private lateinit var actualModel : GameData

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ScreenPageBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.apply {
            back.setOnClickListener {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }

            actualModel = ListPage.getPlayer(page)
            name.text = actualModel.name
            image.setImageResource(actualModel.image)
            desc.text = actualModel.desc

            next.setOnClickListener {
                page++
                actualModel = ListPage.getPlayer(page)
                name.text = actualModel.name
                image.setImageResource(actualModel.image)
                desc.text = actualModel.desc

                if(page == 12){
                    next.visibility = View.GONE
                    toMenu.visibility = View.VISIBLE
                }
            }
            toMenu.setOnClickListener {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}