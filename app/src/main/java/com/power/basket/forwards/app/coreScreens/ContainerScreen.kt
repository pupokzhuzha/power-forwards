package com.power.basket.forwards.app.coreScreens

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.power.basket.forwards.app.R

class ContainerScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.screen_container)
    }
}