package com.power.basket.forwards.app.beginingScreens

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.Incredible.Lakers.team.app.util.replaceActivity
import com.power.basket.forwards.app.R
import com.power.basket.forwards.app.coreScreens.ContainerScreen

@SuppressLint("CustomSplashScreen")
class LoadingScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.screen_loading)

        Handler(Looper.getMainLooper()).postDelayed({
            this.replaceActivity(ContainerScreen())
        }, 500)
    }
}